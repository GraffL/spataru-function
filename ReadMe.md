An implementation of the function (or rather the sequence of functions converging to that function) found by Silvia Spataru in "[An absolutely continuous function whose inverse function is not absolutely continuous](http://siba-ese.unisalento.it/index.php/notemat/article/view/1156)" which has the following properties:
* It is absolutely continuous
* It is strictly increasing (and, thus, invertible)
* It's inverse is not absolutely continuous

An example of such a sequence of functions:

![f_0](images/Spataru-Fct-0.png)
![f_1](images/Spataru-Fct-1.png)
![f_2](images/Spataru-Fct-2.png)
![f_3](images/Spataru-Fct-3.png)
![f_4](images/Spataru-Fct-4.png)
![f_5](images/Spataru-Fct-5.png)