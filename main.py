from __future__ import annotations
import matplotlib.pyplot as plt

# An implementation of the function described by Silvia Sp˘ataru in
#   http://siba-ese.unisalento.it/index.php/notemat/article/view/1156
#   DOI 10.1285/i15900932v23n1p47
# which is
# * absolutely continuous and
# * strictly increasing
# * but its inverse is not absolutely continuous
# More precisely, this code generates the sequence of functions which converges to this function

# Computes the right end of the left most interval J_{n,1}:
def calc_an(n:int, alpha:float):
    assert(n>=0)
    if n==0:
        return 1
    else:
        # In the n-th iteration J_{n-1,1} is partitioned into J_n,1, I_n,1, J_n,2
        # where I_n,1 is the middle part of size alpha^n
        return 0.5*(calc_an(n - 1, alpha) - alpha**n)

# Generate the function f_n
def generatefn(n:int, alpha:float):
    # The measure of set A (a fat Cantor Set)
    lA = alpha / (1 - 2 * alpha)
    assert (lA < 1)

    an = calc_an(n,alpha)

    # The function f_n:
    def fn(x):
        if x <= an:
            # x in J_n,1
            return x*alpha**n/an
        elif x <= an+alpha**n:
            # x in I_n,1
            return fn(an)+1/lA*(x-an)
        else:
            for m in range(1,n+1):
                # x in J_m,2
                if calc_an(m,alpha)+alpha**m <= x <= calc_an(m-1,alpha):
                    return alpha**m*(1+1/lA)+fn(x-calc_an(m,alpha)-alpha**m)
            # x in I_m,1 for some m < n
            return generatefn(n-1,alpha)(x)

    return fn

n = 3
alpha = 0.2
f = generatefn(n,alpha)

N=10000
X = [k/N for k in range(0,N)]
Y = [f(x) for x in X]
plt.plot(X,Y)
plt.axis('equal')
plt.show()